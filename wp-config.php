<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// define URL ---Updated
define( 'WP_HOME', 'http://lab.gbui.cf' );
define( 'WP_SITEURL', 'http://lab.gbui.cf' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gbp-db' );

/** MySQL database username */
define( 'DB_USER', 'gbp-user' );

/** MySQL database password */
define( 'DB_PASSWORD', '1A52o$husw-&+tab?ChU' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         ':)-/ER!OL+O3x4vr-{~+~M=E-QgA~uM;)L&s{OZP5q}_hL9x7os)kZ#:bnz0=2i6');
define('SECURE_AUTH_KEY',  '(M_Cx9anv|Q|zpbfu/y2~j  jg:KeAC,;GQ~#hlE({.-iwMcEVY4T8iQXK]0`o;P');
define('LOGGED_IN_KEY',    ';?e<=linNI:O.8LGL*W:Eg/u[f4jca)D2vL:>uz_9s]&~lcHyZ+^Ue+P@-M&%]-M');
define('NONCE_KEY',        'Y]CNqSZ0B+{gC+/!:;KNmA{+o6m<)#T,Wf06Qt*k[OjKKtN/|q%2(ym:X|> 8j*!');
define('AUTH_SALT',        'tN/0YTi8:2,=u(h`x0CBL4ELO (Aq#Vh[^fP-Q{Osr~;m|FDD<#Bs@4Y(1Og[&PA');
define('SECURE_AUTH_SALT', 'dk?cu~P wiPg.d)WJxX755Z3oJ%|AzsCz0b*G(/+U_0mCaiPRtuq4Ws3>shq0nT>');
define('LOGGED_IN_SALT',   'dbh=]B]|pP$V8]o!V/..-^KX:QY&-.Sp=Fz1#W|0x3fb?(`sXQ--q2bQ?e_q>V6=');
define('NONCE_SALT',       'eJ37:x~A|T$)f!{r.}^qE,Q6A+|>T= |ptm)m)Ii$gS`mpVD+rc~vre!TQpbcfRq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'gbp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
